const axios = require("axios");

module.exports = class JSONPlaceholderController{
    static async getUsers(req,res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users")
           const users = response.data;
           res.status(200).json({message:'Aqui estão os usuários captados da Api pública JSONPlaceholder',users})
        }catch(error){
            console.error(error);
            res.status(500).json({ error: "Falha ao encontrar usuários"});
        }
    }

    static async getUsersWebsiteID(req,res){
        try{
            const response = await axios.get("https://jsonplaceholder.typicode.com/users");
            const users = response.data.filter(
                (user)=> user.website.endsWith(".io")
            )
            
                res.status(200).json({message:"Aqui estão os users com dominio IO",users,});
            
        }
        catch(error){
            console.log(error);
            res.status(500).json({error:"Deu ruim",error})
        }
    }
    static async getUsersWebsiteCOM(req,res){
        try{
          const response = await axios.get(
            "https://jsonplaceholder.typicode.com/users"
          );
          const users = response.data.filter(
            (user) => user.website.endsWith(".com")
          )
          const Banana = users.length
          res
            .status(200)
            .json({
              message:
              "Aqui estão os users com dominio COM, Quantas bananas tem nesse dominio?",Banana,
              users,
            });
        }
        catch(error){
          console.log(error)
          res.status(500).json({ error: "Deu ruim!" });
        }
      }
    
      static async getUsersWebsiteNET(req,res){
        try{
          const response = await axios.get(
            "https://jsonplaceholder.typicode.com/users"
          );
          const users = response.data.filter(
            (user) => user.website.endsWith(".net")
          )
          const Banana = users.length
          res
            .status(200)
            .json({
              message:
              "Aqui estão os users com dominio NET, Quantas bananas tem nesse dominio?",Banana,
              users,
            });
        }
        catch(error){
          console.log(error)
          res.status(500).json({ error: "Deu ruim!" });
        }
      }
}
    




